{
  description = "A server maintenance tool, build to be precise and secure";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

    # inputs for following
    systems = {
      url = "github:nix-systems/x86_64-linux"; # only evaluate for this system
    };
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
    crane = {
      url = "github:ipetkov/crane";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-compat.follows = "flake-compat";
        flake-utils.follows = "flake-utils";
        rust-overlay.follows = "rust-overlay";
      };
    };
    flake-utils = {
      url = "github:numtide/flake-utils";
      inputs = {
        systems.follows = "systems";
      };
    };
    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
      };
    };
  };

  outputs = inputs @ {
    self,
    nixpkgs,
    crane,
    flake-utils,
    rust-overlay,
    ...
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = import nixpkgs {
        inherit system;
        overlays = [(import rust-overlay)];
      };

      module = import ./nix {};
      docs = import ./docs {inherit pkgs module;};

      nightly = true;
      rust =
        if nightly
        then pkgs.rust-bin.selectLatestNightlyWith (toolchain: toolchain.default)
        else pkgs.rust-bin.stable.latest.default;

      craneLib = (crane.mkLib pkgs).overrideToolchain rust;

      craneBuild = craneLib.buildPackage {
        src = craneLib.cleanCargoSource ./.;

        doCheck = true;
        inherit buildInputs nativeBuildInputs;
      };
      buildInputs = with pkgs; [openssl libsodium];
      nativeBuildInputs = with pkgs; [openssl pkg-config];
    in {
      packages = {
        default = self.packages.${system}.rust;
        rust = craneBuild;
        option-docs = docs.optionsDoc;
        docs = docs.documentation;
      };

      app.default = {
        type = "app";
        program = "${self.packages.${system}.default}/bin/sp";
      };

      devShells.default = pkgs.mkShell {
        packages = with pkgs; [
          cocogitto

          rust
          cargo-edit
          cargo-expand

          openssl
        ];

        inherit buildInputs nativeBuildInputs;
      };
    })
    // {
      nixosModules.default = import ./nix inputs;
    };
}
# vim: ts=2

