{pkgs, module}: let
  inherit (pkgs) lib;
  # Generate a MarkDown file describing the options of the NixOS module
  # taken from:
  # https://gitlab.com/simple-nixos-mailserver/nixos-mailserver/-/blob/1bcfcf786bc289ca1bd2c9d29d6f02d9141b1da3/flake.nix
  eval = lib.evalModules {
    modules = [
      module
      {
        _module.check = false;
        services.serverphone = {
          enable = true;
          domains = "example.com";
        };
      }
    ];
  };
  optionsDoc = let
    options = builtins.toFile "options.json" (builtins.toJSON
      (lib.filter (opt: opt.visible && !opt.internal && lib.head opt.loc == "services") # TODO find a better filter

        (lib.optionAttrSetToDocList eval.options)));
  in
    pkgs.runCommand "options.md" {buildInputs = [pkgs.python3Minimal];} ''
      echo "Generating options.md from ${options}"
      python ${./generate-options.py} ${options} > $out
    '';
  documentation = pkgs.stdenv.mkDerivation {
    name = "documentation";
    src = ./.;
    buildInputs = [
      (
        pkgs.python3.withPackages (p:
          with p; [
            sphinx

            # theme
            pydata-sphinx-theme

            myst-parser
            linkify-it-py
          ])
      )
    ];
    buildPhase = ''
      ls -l .;
      cp ${optionsDoc} options.md
      # Workaround for https://github.com/sphinx-doc/sphinx/issues/3451
      unset SOURCE_DATE_EPOCH
      make html
    '';
    installPhase = ''
      cp -Tr _build/html $out
    '';
  };
in {
  inherit documentation;
  inherit optionsDoc;
}
