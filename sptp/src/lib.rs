/// This is a library mostly containing wrapper types for the communication between the
/// serverphone client and server.

pub mod ssh_authorization;
pub mod message;
pub mod error;
pub mod sptp_command;
pub mod certificate_request;
pub mod tls;
