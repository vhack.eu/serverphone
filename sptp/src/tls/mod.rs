use std::{fs::File, io::BufReader, path::Path};

use rustls_pemfile::{certs, read_all, Item};
use tokio_rustls::rustls::{Certificate, PrivateKey};

use crate::error::{CertificateLoadingError, KeyLoadingError};

pub fn load_certs(path: &Path) -> Result<Vec<Certificate>, CertificateLoadingError> {
    let cert_file = File::open(path).map_err(|err| CertificateLoadingError::FileOpen {
        file_path: path.to_owned(),
        err,
    })?;
    let mut certs = certs(&mut BufReader::new(cert_file)).map_err(|err| {
        CertificateLoadingError::CertsExtract {
            file_path: path.to_owned(),
            err,
        }
    })?;
    Ok(certs.drain(..).map(Certificate).collect())
}

pub fn load_keys(path: &Path) -> Result<PrivateKey, KeyLoadingError> {
    let cert_file = File::open(path).map_err(|err| KeyLoadingError::FileOpen {
        file_path: path.to_owned(),
        err,
    })?;
    let mut keys =
        read_all(&mut BufReader::new(cert_file)).map_err(|err| KeyLoadingError::KeyExtract {
            file_path: path.to_owned(),
            err,
        })?;
    let keys: Vec<_> = keys
        .drain(..)
        .map(|item| {
            match item {
                Item::X509Certificate(_) => None, // ignore certificates
                Item::RSAKey(key) => Some(PrivateKey(key)),
                Item::PKCS8Key(key) => Some(PrivateKey(key)),
                Item::ECKey(key) => Some(PrivateKey(key)),
                _ => unimplemented!(), // only accept these encodings for now
            }
        })
        .filter(|value| value.is_some())
        .map(|value| value.expect("We filtered before"))
        .collect();
    if keys.len() > 1 {
        return Err(KeyLoadingError::ToManyKeys());
    } else if keys.len() == 0 {
        return Err(KeyLoadingError::NoKey());
    } else {
        return Ok(keys.get(0).expect("We checked the length above").clone());
    }
}

#[cfg(test)]
mod tests {}
