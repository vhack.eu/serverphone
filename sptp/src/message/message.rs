use serde::{Serialize, Deserialize};

use crate::error;

use super::{byte::{MessageSize, Byte}, package::Package};

#[derive(Serialize, Deserialize, Debug)]
pub struct Message {
    pub(crate) size: MessageSize,
    pub(crate) message: String,
}


impl Into<Message> for String {
    fn into(self) -> Message {
        // TODO what happens when the string length is greater than the u128 max value?

        Message {
            size: self.as_bytes().len() as MessageSize,
            message: self,
        }
    }
}


impl Package for Message {
    fn pack(self) -> Result<String, error::PackToMessageError> {
        let mut size_string: String = self
            .size
            .to_le_bytes()
            .iter()
            .map(|value| -> Byte { value.into() })
            .map(|value| value.to_string())
            .collect();
        size_string.push_str(&self.message);
        Ok(size_string)
    }
}
