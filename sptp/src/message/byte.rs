use std::fmt::Display;

use serde::{Deserialize, Serialize};

pub const MESSAGE_SIZE_LENGHT_BINARY_VALUE: usize = 16 * 8;
pub type MessageSize = u128;

#[derive(Serialize, Deserialize, Debug)]
pub(crate) struct Byte {
    field_128: bool,
    field_64: bool,
    field_32: bool,
    field_16: bool,
    field_8: bool,
    field_4: bool,
    field_2: bool,
    field_1: bool,
}

impl Display for Byte {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        macro_rules! if_bin_check_display {
            ($self:ident.$field:ident, $variable:ident) => {
                if $self.$field {
                    $variable.push('1');
                } else {
                    $variable.push('0');
                }
            };
        }

        let mut output = String::with_capacity(8);
        if_bin_check_display!(self.field_128, output);
        if_bin_check_display!(self.field_64, output);
        if_bin_check_display!(self.field_32, output);
        if_bin_check_display!(self.field_16, output);
        if_bin_check_display!(self.field_8, output);
        if_bin_check_display!(self.field_4, output);
        if_bin_check_display!(self.field_2, output);
        if_bin_check_display!(self.field_1, output);
        f.write_str(&output)
    }
}

impl Into<Byte> for u8 {
    fn into(mut self) -> Byte {
        macro_rules! if_bin_check {
            ($value:expr,$field:ident, $variable:ident) => {
                if $variable >= $value {
                    $field = true;
                    $variable -= $value;
                }
            };
        }

        let mut field_128 = false;
        let mut field_64 = false;
        let mut field_32 = false;
        let mut field_16 = false;
        let mut field_8 = false;
        let mut field_4 = false;
        let mut field_2 = false;
        let mut field_1 = false;

        if_bin_check!(128, field_128, self);
        if_bin_check!(64, field_64, self);
        if_bin_check!(32, field_32, self);
        if_bin_check!(16, field_16, self);
        if_bin_check!(8, field_8, self);
        if_bin_check!(4, field_4, self);
        if_bin_check!(2, field_2, self);
        if_bin_check!(1, field_1, self);

        assert_eq!(self, 0);
        Byte {
            field_128,
            field_64,
            field_32,
            field_16,
            field_8,
            field_4,
            field_2,
            field_1,
        }
    }
}
impl Into<Byte> for &u8 {
    fn into(self) -> Byte {
        <u8 as Into<Byte>>::into(*self)
    }
}

impl Into<u8> for Byte {
    fn into(self) -> u8 {
        macro_rules! into_check {
            ($self:ident.$field:ident, $output:ident, $value:expr) => {
                if $self.$field {
                    $output += $value;
                }
            };
        }
        let mut output = 0;
        into_check!(self.field_128, output, 128);
        into_check!(self.field_64, output, 64);
        into_check!(self.field_32, output, 32);
        into_check!(self.field_16, output, 16);
        into_check!(self.field_8, output, 8);
        into_check!(self.field_4, output, 4);
        into_check!(self.field_2, output, 2);
        into_check!(self.field_1, output, 1);
        output
    }
}

impl From<String> for Byte {
    fn from(value: String) -> Self {
        macro_rules! check_for_char {
            ($index:literal, $field:ident, $chars:ident) => {
                if $chars[$index] == '1' {
                    $field = true;
                } else if $chars[$index] == '0' {
                    $field = false;
                } else {
                    unreachable!("Binary strings only contain 0s and 1s!")
                }
            };
        }

        assert_eq!(value.len(), 8); // one byte is 8 bit
        let chars: Vec<_> = value.chars().collect();

        let field_128;
        let field_64;
        let field_32;
        let field_16;
        let field_8;
        let field_4;
        let field_2;
        let field_1;

        check_for_char!(0, field_128, chars);
        check_for_char!(1, field_64, chars);
        check_for_char!(2, field_32, chars);
        check_for_char!(3, field_16, chars);
        check_for_char!(4, field_8, chars);
        check_for_char!(5, field_4, chars);
        check_for_char!(6, field_2, chars);
        check_for_char!(7, field_1, chars);

        Byte {
            field_128,
            field_64,
            field_32,
            field_16,
            field_8,
            field_4,
            field_2,
            field_1,
        }
    }
}

#[cfg(test)]
mod test {
    use std::borrow::BorrowMut;

    use crate::message::{byte::{Byte, MessageSize, MESSAGE_SIZE_LENGHT_BINARY_VALUE}, package::Package};

    #[test]
    fn test_for_message_size() {
        let message = String::from("Hi").pack().unwrap();

        let mut binary_stream = message.chars().take(MESSAGE_SIZE_LENGHT_BINARY_VALUE);
        let mut binary_message_length = [0; MESSAGE_SIZE_LENGHT_BINARY_VALUE / 8];

        for i in 0..(MESSAGE_SIZE_LENGHT_BINARY_VALUE / 8) {
            let byte_string: String = binary_stream.borrow_mut().take(8).collect();
            let byte_as_number: u8 = Byte::from(byte_string).into();
            binary_message_length[i] = byte_as_number;
        }

        let number: MessageSize = MessageSize::from_le_bytes(binary_message_length);

        assert_eq!(number, 2);
    }

    #[test]
    fn test_binary_conversion() {
        // 137
        //
        // 137 - 128 = 1
        // 009 - 064 = 0
        // 009 - 032 = 0
        // 009 - 016 = 0
        // 009 - 008 = 1
        // 001 - 004 = 0
        // 001 - 002 = 0
        // 001 - 001 = 1
        // => 10001001

        let number: u8 = 137;
        let bin_number: Byte = number.into();
        assert_eq!("10001001".to_owned(), bin_number.to_string());
    }
}
