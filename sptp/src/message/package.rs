use std::borrow::BorrowMut;

use async_trait::async_trait;
use serde::de::DeserializeOwned;
use tokio::io::{AsyncRead, AsyncReadExt, AsyncWrite, AsyncWriteExt};

use crate::{error, message::byte::Byte};

use super::{
    byte::{MessageSize, MESSAGE_SIZE_LENGHT_BINARY_VALUE},
    message::Message,
    status::Status,
};

pub trait Package {
    fn pack(self) -> Result<String, error::PackToMessageError>;
}

#[async_trait]
pub trait PackageExt {
    async fn send_as_package<W: AsyncWrite + std::marker::Unpin + std::marker::Send>(
        self,
        writer: &mut W,
    ) -> Result<(), error::PackToMessageError>;
}

#[async_trait]
impl<T: std::marker::Sized + Package + std::marker::Send> PackageExt for T {
    async fn send_as_package<W: AsyncWrite + std::marker::Unpin + std::marker::Send>(
        self,
        writer: &mut W,
    ) -> Result<(), error::PackToMessageError> {
        let package = self.pack()?;

        writer
            .write_all(package.as_bytes())
            .await
            .map_err(|err| error::PackToMessageError::Write(err))?;

        writer
            .flush()
            .await
            .map_err(|err| error::PackToMessageError::Write(err))?;
        Ok(())
    }
}

#[async_trait]
pub trait GetNextMessage {
    async fn next_message(&mut self) -> Result<String, error::TurnToMessageError>;
    async fn next_message_deserialized<T>(&mut self) -> Result<T, error::TurnToMessageError>
    where
        T: DeserializeOwned,
    {
        let message = self.next_message().await?;
        Ok(ron::from_str(&message)
            .expect("This was serialized, it should work the other way around"))
    }
}

#[async_trait]
impl<R: AsyncRead + std::marker::Unpin + std::marker::Send> GetNextMessage for R {
    async fn next_message(&mut self) -> Result<String, error::TurnToMessageError> {
        let mut binary_length_message = [0; MESSAGE_SIZE_LENGHT_BINARY_VALUE];
        self.read_exact(&mut binary_length_message).await?;

        let length_message = String::from_utf8(binary_length_message.to_vec())
            .expect("This has been serialized, thus should be valid utf-8");

        let mut binary_message_length = [0; MESSAGE_SIZE_LENGHT_BINARY_VALUE / 8];
        let mut binary_stream = length_message.chars();

        for i in 0..(MESSAGE_SIZE_LENGHT_BINARY_VALUE / 8) {
            let byte_string: String = binary_stream.borrow_mut().take(8).collect();
            let byte_as_number: u8 = Byte::from(byte_string).into();
            binary_message_length[i] = byte_as_number;
        }

        let length: MessageSize = MessageSize::from_le_bytes(binary_message_length);

        let mut buffer = vec![0; length as usize];
        self.read_exact(&mut buffer[..]).await?;

        let message_content =
            String::from_utf8(buffer).expect("This was once utf-8, thus converting should work");

        //dbg!(&message_content);
        let status_message: Option<Status> = match ron::from_str(&message_content) {
            Ok(tmp) => Some(tmp),
            Err(_) => None,
        };

        if let Some(status) = status_message {
            match status {
                Status::SshVerificationFailed => {
                    return Err(error::TurnToMessageError::ServerError(
                        Status::SshVerificationFailed,
                    ));
                }
                Status::PasswordVerificationFailed => {
                    return Err(error::TurnToMessageError::ServerError(
                        Status::PasswordVerificationFailed,
                    ));
                }
                Status::ConnectionFailed(err) => {
                    return Err(error::TurnToMessageError::ServerError(
                        Status::ConnectionFailed(err),
                    ));
                }
                Status::ConnectionSucceeded
                | Status::CommandOutputEnd
                | Status::CommandOutputStart => {
                    return Ok(message_content);
                }
            };
        } else {
            return Ok(message_content);
        }
    }
}

impl<'a> Package for &'a str {
    fn pack(self) -> Result<String, error::PackToMessageError> {
        let message: Message = self.to_owned().into();
        message.pack()
    }
}
impl Package for String {
    fn pack(self) -> Result<String, error::PackToMessageError> {
        let message: Message = self.into();
        message.pack()
    }
}

#[cfg(test)]
mod test {
    use tokio::{io::AsyncWriteExt, net::UnixStream};

    use crate::message::package::{GetNextMessage, Package};

    #[tokio::test]
    async fn test_read_next_message() {
        let (stream1, stream2) = UnixStream::pair().unwrap();
        let (_, mut writer) = stream1.into_split();
        let (mut reader2, _) = stream2.into_split();

        let message = "Hi!".to_owned().pack().unwrap();
        writer.write_all(message.as_bytes()).await.unwrap();
        writer.flush().await.unwrap();
        assert_eq!(reader2.next_message().await.unwrap(), "Hi!".to_owned());
    }

    #[tokio::test]
    async fn test_read_large_message() {
        let (stream1, stream2) = UnixStream::pair().unwrap();
        let (_, mut writer) = stream1.into_split();
        let (mut reader2, _) = stream2.into_split();

        let message = "This is a lot of text, multiple bytes to be precise, also it should be going over multiple lines. I assume that this is enought to prove my point, that the current messaging implementation will fail with larger messages.".to_owned();
        writer
            .write_all(message.clone().pack().unwrap().as_bytes())
            .await
            .unwrap();
        writer.flush().await.unwrap();
        assert_eq!(reader2.next_message().await.unwrap(), message);
    }
}
