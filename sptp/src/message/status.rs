use std::fmt::Display;

use serde::{Deserialize, Serialize};

use super::package::Package;

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, PartialOrd, Ord, Eq)]
pub enum Status {
    SshVerificationFailed,
    PasswordVerificationFailed,
    ConnectionFailed(String),
    ConnectionSucceeded,
    CommandOutputStart,
    CommandOutputEnd,
}

impl Display for Status {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Status::SshVerificationFailed => f.write_str("Ssh verification failed"),
            Status::PasswordVerificationFailed => f.write_str("Password verification failed"),
            Status::ConnectionFailed(err) => {
                f.write_str(&format!("Connection failed, see error:\n {err}"))
            }
            Status::ConnectionSucceeded => f.write_str("Connection Succeeded"),
            Status::CommandOutputStart => f.write_str("Start of command output"),
            Status::CommandOutputEnd => f.write_str("End of command output"),
        }
    }
}

impl Package for Status {
    fn pack(self) -> Result<String, crate::error::PackToMessageError> {
        ron::to_string(&self).expect("This serializes hard coded values, it should always work")[..]
            .pack()
    }
}

#[cfg(test)]
mod test {
    use tokio::{io::AsyncWriteExt, net::UnixStream};

    use crate::{
        error,
        message::{
            package::{GetNextMessage, Package},
            status::Status,
        },
    };

    #[tokio::test]
    async fn test_serialize_and_deserialize() {
        let (stream1, stream2) = UnixStream::pair().unwrap();
        let (_, mut writer) = stream1.into_split();
        let (mut reader2, _) = stream2.into_split();

        let message = Status::SshVerificationFailed.pack().unwrap();

        writer.write_all(message.as_bytes()).await.unwrap();

        writer.flush().await.unwrap();

        let result = reader2.next_message().await;
        assert!(result.is_err());
        let error = result.unwrap_err();
        match error {
            error::TurnToMessageError::Read(_) => unreachable!(),
            error::TurnToMessageError::ServerError(status) => {
                assert_eq!(status, Status::SshVerificationFailed);
            }
        };
    }
}
