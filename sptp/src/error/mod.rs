use std::path::PathBuf;

use thiserror::Error;
use tokio::io;

use crate::message::status::Status;

#[derive(Debug, Error)]
pub enum PackToMessageError {
    #[error("Failed to serialize some data")]
    Serializing(#[from] ron::Error),

    #[error("Failed to write some data")]
    Write(#[from] io::Error),
}

#[derive(Debug, Error)]
pub enum TurnToMessageError {
    #[error("Failed to read from the internal tcp stream")]
    Read(#[from] io::Error),

    #[error("The server returned a error instead of the expected answer: {0}")]
    ServerError(Status),
}

#[derive(Debug, Error)]
pub enum UserParsing {
    #[error("The supplied string does not contain enought withspace seperated entries")]
    ToShort(),
    #[error("The supplied string contain to much withspace seperated entries")]
    ToLong(),
    #[error("Failed to parse the string")]
    PasswordEncoding(#[from] argon2::password_hash::Error),
}

#[derive(Debug, Error)]
pub enum FulfillRequest {
    #[error("Could not parse pem string")]
    FromPem(#[from] rcgen::RcgenError),
}


#[derive(Debug, Error)]
pub enum CertificateLoadingError {
    #[error("Failed to open cert file: {file_path}")]
    FileOpen {
        file_path: PathBuf,
        err: io::Error,
    },

    #[error("Failed to extract certificates from file: {file_path}")]
    CertsExtract {
        file_path: PathBuf,
        err: io::Error,
    },
}

#[derive(Debug, Error)]
pub enum KeyLoadingError {
    #[error("Failed to open key file: {file_path}")]
    FileOpen {
        file_path: PathBuf,
        err: io::Error,
    },

    #[error("Failed to extract key from file: {file_path}")]
    KeyExtract {
        file_path: PathBuf,
        err: io::Error,
    },
    #[error("More than one key found in key file, please only add one")]
    ToManyKeys(),

    #[error("Did not find a key in your file, are you sure that it is encoding a key?")]
    NoKey(),
}
