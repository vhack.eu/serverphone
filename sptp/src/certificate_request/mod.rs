use std::str::FromStr;

use argon2::{password_hash::PasswordHashString, Argon2, PasswordHash, PasswordVerifier};
use rcgen::{Certificate, CertificateParams, CertificateSigningRequest, KeyPair};
use serde::{Deserialize, Serialize};
use time::{ext::NumericalDuration, OffsetDateTime};

use crate::{error::{self, FulfillRequest}, message::package::Package};

#[derive(Serialize, Deserialize)]
pub struct CertificateRequest {
    certificate_request_pem: String,
}

impl Package for CertificateRequest {
    fn pack(self) -> Result<String, error::PackToMessageError> {
        ron::to_string(&self).expect("This serializes hard coded values, it should always work")[..]
            .pack()
    }
}

impl CertificateRequest {
    pub fn new(certificate_request_pem: String) -> Self {
        Self {
            certificate_request_pem,
        }
    }

    pub fn fulfill_request(
        self,
        ca_certificate_pem: &String,
        ca_certificate_private_key_pem: &String,
        duration: i64,
    ) -> Result<String, FulfillRequest> {
        let mut certificate_signing_request =
            CertificateSigningRequest::from_pem(&self.certificate_request_pem)?;

        let time_now = OffsetDateTime::now_utc();

        certificate_signing_request.params.not_after = time_now.saturating_add(duration.days());
        certificate_signing_request.params.not_before = time_now;

        let ca_certificate_key = KeyPair::from_pem(&ca_certificate_private_key_pem)?;

        let ca_certificate_params =
            CertificateParams::from_ca_cert_pem(&ca_certificate_pem, ca_certificate_key)?;
        let ca_certificate = Certificate::from_params(ca_certificate_params)?;

        let signed_certificate_str =
            certificate_signing_request.serialize_pem_with_signer(&ca_certificate)?;

        Ok(signed_certificate_str)
    }
}

#[derive(Serialize, Deserialize)]
pub struct PasswordAuthorizationRequest {
    username: String,
    password: String,
}

impl Package for PasswordAuthorizationRequest {
    fn pack(self) -> Result<String, error::PackToMessageError> {
        ron::to_string(&self).expect("This serializes hard coded values, it should always work")[..]
            .pack()
    }
}

impl PasswordAuthorizationRequest {
    pub fn new(username: String, password: String) -> Self {
        Self { username, password }
    }
    pub fn verify_request(&self, allowed_users: &Vec<User>) -> bool {
        allowed_users.iter().any(|user| {
            (user.username == self.username)
                && Argon2::default()
                    .verify_password(
                        self.password.as_bytes(),
                        &user.password_hash.password_hash(),
                    )
                    .is_ok()
        })
    }
}

pub struct User {
    username: String,
    password_hash: PasswordHashString,
}

impl User {
    pub fn new(username: String, password_hash: PasswordHashString) -> Self {
        Self {
            username,
            password_hash,
        }
    }
}
impl FromStr for User {
    type Err = error::UserParsing;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let split_string: Vec<_> = s.split_ascii_whitespace().collect();
        if split_string.len() != 2 {
            if split_string.len() < 2 {
                return Err(error::UserParsing::ToShort());
            } else {
                return Err(error::UserParsing::ToLong());
            }
        } else {
            let username = split_string.get(0).expect("We checked the length above");
            let password_hash_string = split_string.get(1).expect("We checked the length above");
            let password_hash = PasswordHash::new(password_hash_string)?;
            Ok(User {
                username: username.to_string(),
                password_hash: password_hash.serialize(),
            })
        }
    }
}

#[cfg(test)]
mod tests {}
