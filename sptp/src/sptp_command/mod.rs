use serde::{Deserialize, Serialize};

use crate::message::package::Package;

/// Order the server to do something
#[derive(Debug, Serialize, Deserialize)]
pub enum SptpCommand {
    /// Deploy the newest configuration, i.e., clone the configuration
    /// repository and rebuild the system
    Deploy,
}

impl Package for SptpCommand {
    fn pack(self) -> Result<String, crate::error::PackToMessageError> {
        ron::to_string(&self).expect("This serializes hard coded values, it should always work")[..]
            .pack()
    }
}

/// Were a command output line originated
#[derive(Debug, Serialize, Deserialize)]
pub enum SptpCommandOutput {
    Stdout(String),
    Stderr(String),
}

impl Package for SptpCommandOutput {
    fn pack(self) -> Result<String, crate::error::PackToMessageError> {
        ron::to_string(&self).expect("This serializes hard coded values, it should always work")[..]
            .pack()
    }
}
