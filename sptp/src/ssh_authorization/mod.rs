use std::str::FromStr;

use rand::{rngs::StdRng, RngCore, SeedableRng};
use serde::{Deserialize, Serialize};
use thrussh_keys::{
    key::{KeyPair, PublicKey},
    signature::Signature,
};

use crate::message::package::Package;

#[derive(Debug)]
pub struct SshAuthorization<'a> {
    // size limited because of const generics support in serde (32 is max)
    // see: https://github.com/serde-rs/serde/issues/1937
    random_value: [u8; 32],
    accepted_ssh_keys: &'a Vec<PublicKey>,
}

impl<'a> SshAuthorization<'a> {
    pub fn new(accepted_ssh_keys: &'a Vec<PublicKey>) -> Self {
        let mut rng = StdRng::from_entropy();
        let mut random_value = [0; 32];
        rng.fill_bytes(&mut random_value[..]);

        SshAuthorization {
            random_value,
            accepted_ssh_keys,
        }
    }
    /// Generate a challenge from the struct and serialize it with ron to a string
    pub fn generate_challenge(&self) -> String {
        let challenge = SshAuthorizationChallenge {
            random_value: self.random_value,
        };
        ron::to_string(&challenge).expect("This is constructed and should always be serializable")
    }

    /// Check if an answer actually contains the correct signature
    pub fn validate_answer(&self, answer: &SshAuthorizationAnswer) -> bool {
        self.accepted_ssh_keys
            .iter()
            .any(|key| key.verify_detached(&self.random_value, &answer.signed_random_value))
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct SshAuthorizationChallenge {
    random_value: [u8; 32],
}
impl Package for SshAuthorizationChallenge {
    fn pack(self) -> Result<String, crate::error::PackToMessageError> {
        ron::to_string(&self).expect("This serializes hard coded values, it should always work")[..]
            .pack()
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct SshAuthorizationAnswer {
    random_value: [u8; 32],
    signed_random_value: Vec<u8>,
}
impl Package for SshAuthorizationAnswer  {
    fn pack(self) -> Result<String, crate::error::PackToMessageError> {
        ron::to_string(&self).expect("This serializes hard coded values, it should always work")[..]
            .pack()
    }
}

impl FromStr for SshAuthorizationChallenge {
    type Err = ron::error::SpannedError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        ron::from_str(s)
    }
}
impl From<String> for SshAuthorizationChallenge {
    fn from(value: String) -> Self {
        SshAuthorizationChallenge::from_str(&value)
            .expect("This should be deserializable, as it was serialized")
    }
}

impl FromStr for SshAuthorizationAnswer {
    type Err = ron::error::SpannedError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        ron::from_str(s)
    }
}

impl From<String> for SshAuthorizationAnswer {
    fn from(value: String) -> Self {
        SshAuthorizationAnswer::from_str(&value)
            .expect("This should be deserializable, as it was serialized")
    }
}

impl SshAuthorizationChallenge {
    /// Fulfill a ssh authorization challenge, by singing the value
    /// with a private key. Will return a serialized response
    pub fn fulfill_challenge_with_private_key(
        &self,
        key: KeyPair,
    ) -> Result<String, thrussh_keys::Error> {
        let signed_random_value = match key.sign_detached(&self.random_value)? {
            Signature::Ed25519(signature_bytes) => signature_bytes.0.to_vec(),
            Signature::RSA { hash: _, bytes } => bytes,
            Signature::P256(bytes) => bytes,
        };
        let answer = SshAuthorizationAnswer {
            random_value: self.random_value,
            signed_random_value,
        };
        Ok(ron::to_string(&answer)
            .expect("This has been constructed, it should never be not serializable"))
    }
    /// Fulfill a ssh authorization challenge, by accepting the supplied value.
    /// Will return a serialized  response
    pub fn fulfill_challenge_with_supplied_value(
        &self,
        signed_random_value: Vec<u8>,
    ) -> Result<String, thrussh_keys::Error> {
        let answer = SshAuthorizationAnswer {
            random_value: self.random_value,
            signed_random_value,
        };
        Ok(ron::to_string(&answer)
            .expect("This has been constructed, it should never be not serializable"))
    }

    pub fn random_value(&self) -> [u8; 32] {
        self.random_value
    }
}
#[cfg(test)]
mod tests {
    use thrussh_keys::key::KeyPair;

    use super::{SshAuthorization, SshAuthorizationAnswer, SshAuthorizationChallenge};

    #[test]
    fn test_working_key_pair() {
        let key_pair = KeyPair::generate_ed25519().unwrap();
        let keys = vec![key_pair.clone_public_key()];

        let ssh_authorization = SshAuthorization::new(&keys);

        let challenge: SshAuthorizationChallenge = ssh_authorization.generate_challenge().into();
        let answer: SshAuthorizationAnswer = challenge
            .fulfill_challenge_with_private_key(key_pair)
            .unwrap()
            .into();
        assert!(ssh_authorization.validate_answer(&answer));
    }

    #[test]
    fn test_failing_key_pair() {
        let key_pair = KeyPair::generate_ed25519().unwrap();
        let key_pair2 = KeyPair::generate_ed25519().unwrap();
        let keys = vec![key_pair2.clone_public_key()];

        let ssh_authorization = SshAuthorization::new(&keys);

        let challenge: SshAuthorizationChallenge = ssh_authorization.generate_challenge().into();
        let answer: SshAuthorizationAnswer = challenge
            .fulfill_challenge_with_private_key(key_pair)
            .unwrap()
            .into();
        assert!(!ssh_authorization.validate_answer(&answer));
    }
}
