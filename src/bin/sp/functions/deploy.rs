use anyhow::{Context, Result};
use sptp::{message::package::PackageExt, sptp_command::SptpCommand};
use std::{net::SocketAddr, sync::Arc};
use tokio::io::split;
use tokio_rustls::rustls::{ClientConfig, ServerName};

use crate::types::tls_client::TlsClient;

use super::{read_command_output, shutdown_stream, ssh_authorize_connection};

pub async fn deploy(
    addr: SocketAddr,
    config: Arc<ClientConfig>,
    server_name: ServerName,
    public_key_fingerprint: Option<String>,
) -> Result<()> {
    let client = TlsClient::new(addr, server_name.clone(), config)
        .await
        .context("Failed to set up TlsClient")?;

    let (mut reader, mut writer) = split(client.stream);
    ssh_authorize_connection(&mut reader, &mut writer, public_key_fingerprint)
        .await
        .context("Failed to answer server request to authorize the connection through ssh")?;

    SptpCommand::Deploy
        .send_as_package(&mut writer)
        .await
        .context("Failed to send command")?;

    read_command_output(&mut reader)
        .await
        .context("Failed to read command output")?;

    shutdown_stream(reader, writer)
        .await
        .context("Failed to shutdown the connection")?;
    Ok(())
}
