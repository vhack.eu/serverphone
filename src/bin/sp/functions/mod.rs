use anyhow::{anyhow, Context, Result};
use sptp::{
    error::TurnToMessageError,
    message::{
        package::{GetNextMessage, Package},
        status::Status,
    },
    sptp_command::SptpCommandOutput,
    ssh_authorization::SshAuthorizationChallenge,
};
use thrussh_keys::{agent::client::AgentClient, signature::Signature};
use tokio::io::{AsyncRead, AsyncWrite, AsyncWriteExt, ReadHalf, WriteHalf};

pub mod deploy;
pub mod request_certificate;

pub async fn ssh_authorize_connection<
    R: AsyncRead + std::marker::Unpin + GetNextMessage + std::marker::Send,
    W: AsyncWrite + std::marker::Unpin,
>(
    reader: &mut R,
    writer: &mut W,
    public_key_fingerprint: Option<String>,
) -> Result<()> {
    let challenge: SshAuthorizationChallenge = reader
        .next_message_deserialized()
        .await
        .context("Failed to read challenge from server")?;

    let mut agent = AgentClient::connect_env()
        .await
        .context("Failed to connect to ssh agent")?;

    let identities = agent
        .request_identities()
        .await
        .context("Failed to request identities for agent")?;

    let public_key = if identities.len() == 1 {
        identities.get(0).expect("We checked for the length above")
    } else if identities.len() == 0 {
        return Err(anyhow!(
            "Need at least one key in the agent to sign the request!"
        ));
    } else {
        let public_key_fingerprint = if let Some(pkfp) = public_key_fingerprint {
            pkfp
        } else {
            return Err(anyhow!(
                "Need a public key fingerprint, if the agent contains multiple identities"
            ));
        };
        let matched_identities: Vec<_> = identities
            .iter()
            .filter(|public_key| public_key.fingerprint() == public_key_fingerprint)
            .collect();
        matched_identities
            .get(0)
            .with_context(|| {
                format!(
                    "Could not find key with fingerprint: {}",
                    public_key_fingerprint
                )
            })?
            .to_owned()
    };

    let (_, maybe_signed_challenge) = agent
        .sign_request_signature(public_key, &challenge.random_value()[..])
        .await;
    let signed_challenge =
        match maybe_signed_challenge.context("Failed to let agent sign challenge")? {
            Signature::Ed25519(signature_bytes) => signature_bytes.0.to_vec(),
            Signature::RSA { hash: _, bytes } => bytes,
            Signature::P256(bytes) => bytes,
        };

    let answer = challenge
        .fulfill_challenge_with_supplied_value(signed_challenge)
        .context("Failed to fulfill challenge")?;
    writer
        .write_all(
            answer
                .pack()
                .context("Failed to package string")?
                .as_bytes(),
        )
        .await
        .context("Failed to write answer")?;
    writer.flush().await.context("Failed to flush stream")?;

    Ok(())
}

async fn shutdown_stream<T>(mut reader: ReadHalf<T>, writer: WriteHalf<T>) -> Result<()>
where
    T: std::marker::Unpin + AsyncRead + std::marker::Send + AsyncWrite,
{
    let connection_success: Result<Status, _> = reader.next_message_deserialized().await;
    match connection_success {
        Ok(ok) => {
            // all other messages here would be a protocol error
            assert_eq!(ok, Status::ConnectionSucceeded);
        }
        Err(err) => {
            if let TurnToMessageError::ServerError(status) = &err {
                match status {
                    Status::SshVerificationFailed
                    | Status::PasswordVerificationFailed
                    | Status::ConnectionFailed(_) => {
                        return Err(err).context("The connection to the server failed");
                    }
                    Status::ConnectionSucceeded
                    | Status::CommandOutputEnd
                    | Status::CommandOutputStart => unreachable!("Filtered out by `next_message`"),
                };
            } else {
                return Err(err).context("Failed to read success message");
            }
        }
    };
    let mut stream = reader.unsplit(writer);
    // as the server already sent his success message, closing this stream is optional. The server
    // might have even closed it.
    let _ = stream.shutdown().await;
    Ok(())
}

async fn read_command_output<
    R: AsyncRead + std::marker::Unpin + GetNextMessage + std::marker::Send,
>(
    reader: &mut R,
) -> Result<()> {
    eprintln!("Recieving output from server..");
    let command_output_start: Status = reader
        .next_message_deserialized()
        .await
        .context("Failed to read next message")?;
    assert_eq!(command_output_start, Status::CommandOutputStart);
    let mut reading = true;
    while reading {
        let line = reader
            .next_message()
            .await
            .context("Failed to read next message")?;
        if let Ok(Status::CommandOutputEnd) = ron::from_str(&line) {
            reading = false;
        } else if let Ok(SptpCommandOutput::Stdout(line)) = ron::from_str(&line) {
            eprintln!("[stdout]: {}", line);
        } else if let Ok(SptpCommandOutput::Stderr(line)) = ron::from_str(&line) {
            eprintln!("[stderr]: {}", line);
        } else {
            unreachable!("everything else would be a protocol error");
        }
    }
    eprintln!("Server output finished");
    Ok(())
}
