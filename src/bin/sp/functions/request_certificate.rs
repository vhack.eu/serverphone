use std::{net::SocketAddr, path::Path, sync::Arc};

use anyhow::{Context, Result};
use rcgen::{Certificate, CertificateParams, DnType, SignatureAlgorithm};
use rpassword::prompt_password;
use sptp::{
    certificate_request::{CertificateRequest, PasswordAuthorizationRequest},
    message::package::{GetNextMessage, PackageExt},
};
use tokio::{
    fs,
    io::{split, stdin, stdout, AsyncBufReadExt, AsyncWrite, AsyncWriteExt, BufReader},
};
use tokio_rustls::rustls::{ClientConfig, ServerName};

use crate::types::tls_client::TlsClient;

use super::{shutdown_stream, ssh_authorize_connection};

pub async fn request_certificate(
    addr: SocketAddr,
    config: Arc<ClientConfig>,
    server_name: ServerName,
    public_key_fingerprint: Option<String>,
    key_out: &Path,
    cert_out: &Path,
) -> Result<()> {
    let client = TlsClient::new(addr, server_name.clone(), config)
        .await
        .context("Failed to set up TlsClient")?;

    let (mut reader, mut writer) = split(client.stream);
    ssh_authorize_connection(&mut reader, &mut writer, public_key_fingerprint)
        .await
        .context("Failed to answer server request to authorize the connection through ssh")?;
    password_authorized_connection(&mut writer)
        .await
        .context("Failed to authorized via password")?;

    let (private_key, certificate_request) = generate_certificate_request_and_key()
        .context("Failed to generate certificate and privat key")?;

    CertificateRequest::new(certificate_request)
        .send_as_package(&mut writer)
        .await
        .context("Failed to write certificate request")?;

    let certificate = reader
        .next_message()
        .await
        .context("Failed to read signed certificate")?;

    fs::write(cert_out, certificate.as_bytes())
        .await
        .context("Failed to write certificate to file")?;
    fs::write(key_out, private_key.as_bytes())
        .await
        .context("Failed to write private key to file")?;

    shutdown_stream(reader, writer)
        .await
        .context("Failed to shutdown the connection")?;
    Ok(())
}

fn generate_certificate_request_and_key() -> Result<(String, String)> {
    const PKCS_ECDSA_P256_SHA256_OID: &[u64] = &[1, 2, 840, 10045, 4, 3, 2];
    let algorithm = SignatureAlgorithm::from_oid(PKCS_ECDSA_P256_SHA256_OID)?;

    // subject alternative names are irrelevant:
    let mut certificate_params = CertificateParams::new(vec![]);

    certificate_params.alg = algorithm;
    certificate_params
        .distinguished_name
        .push(DnType::CommonName, "Serverphone client");

    let certificate = Certificate::from_params(certificate_params)?;
    let secret_key_str = certificate.serialize_private_key_pem();
    let certificate_str = certificate.serialize_request_pem()?;

    Ok((secret_key_str, certificate_str))
}

async fn password_authorized_connection<W: AsyncWrite + std::marker::Unpin + std::marker::Send>(
    mut writer: &mut W,
) -> Result<()> {
    let (stdin, mut stdout) = (stdin(), stdout());
    stdout
        .write_all(b"Your username: ")
        .await
        .context("Failed to write username prompt")?;
    stdout.flush().await.context("Failed to flush writer")?;

    let mut username = String::new();
    BufReader::new(stdin)
        .read_line(&mut username)
        .await
        .context("Failed to read username")?;
    let username = username.strip_suffix('\n').expect(
        "All cli input should have a newline,
        otherwise the read command above would have never returned something",
    ).to_owned();

    let password = prompt_password("Your password: ").context("Failed to read password")?;
    PasswordAuthorizationRequest::new(username, password)
        .send_as_package(&mut writer)
        .await
        .context("Failed to write password authorization request")?;
    Ok(())
}
