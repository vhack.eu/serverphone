use anyhow::Result;
use clap::Parser;
use sptp::tls::{load_certs, load_keys};
use states::{authorized, no_certificate};
use types::cli::{Args, Command};

pub mod functions;
pub mod states;
pub mod types;

#[tokio::main]
async fn main() -> Result<()> {
    let args = Args::parse();

    match args.command.clone() {
        Command::Authorized {
            command,
            certificate,
            private_key,
        } => {
            let certificates = load_certs(&certificate)?;
            let key = load_keys(&private_key)?;
            return authorized::start_client(args, command, certificates, key).await;
        }
        Command::RequestCertificate {
            key_output_file,
            certificate_output_file,
        } => {
            return no_certificate::start_client(args, key_output_file, certificate_output_file)
                .await;
        }
    }
}
