use std::{fs::File, io::BufReader, path::PathBuf, sync::Arc};

use anyhow::{Context, Result};
use futures::{stream, StreamExt};
use rustls_pemfile::certs;
use tokio::net;
use tokio_rustls::rustls::{self, ClientConfig, OwnedTrustAnchor, RootCertStore, ServerName};
use webpki_roots::TLS_SERVER_ROOTS;

use crate::{
    functions::request_certificate::request_certificate,
    types::cli::{self, IpVersion},
};

pub async fn start_client(args: cli::Args, key_out: PathBuf, cert_out: PathBuf) -> Result<()> {
    let config = Arc::new(
        generate_client_config(args.ca_file)
            .await
            .context("Failed to get client config")?,
    );

    let join_handles: Vec<_> =
        args.hosts
            .into_iter()
            .map(|(domain, port)| {
                let port = port.unwrap_or(9311);
                let local_config = Arc::clone(&config);
                let public_key_fingerprint = args.public_key_fingerprint.clone();
                let local_cert_out = cert_out.clone();
                let local_key_out = key_out.clone();

                let future = async move {
                    let addrs = net::lookup_host((domain.clone(), port))
                        .await
                        .with_context(|| {
                            format!(
                                "Failed to look up address for host '{}' on port '{}'",
                                domain, port
                            )
                        })?;
                    let server_name = ServerName::try_from(&domain[..]).with_context(|| {
                        format!("Parsing host name ('{}') as server name failed", domain)
                    })?;
                    let socket_addr = addrs
                    .filter(|addr| match args.ip_version {
                        IpVersion::V4 => addr.is_ipv4(),
                        IpVersion::V6 => addr.is_ipv6(),
                    })
                    .next()
                    .expect(
                        "There should never be more than one address, as thats how domains work");
                    // If the domain has both a ip V6 and ip V4 address

                    request_certificate(
                        socket_addr,
                        local_config,
                        server_name,
                        public_key_fingerprint,
                        &local_key_out,
                        &local_cert_out,
                    ).await.context("Failed to request certificate")
                };
                tokio::spawn(future)
            })
            .collect();
    let mut result_stream = stream::iter(join_handles)
        .map(|handle| async move { handle.await })
        .buffer_unordered(100);
    while let Some(join_result) = result_stream.next().await {
        join_result.context("Failed to join task")??
    }

    Ok(())
}
async fn generate_client_config(ca_file: Option<PathBuf>) -> Result<ClientConfig> {
    let mut root_store = RootCertStore::empty();

    if let Some(ca_file) = ca_file {
        let mut cert_file = BufReader::new(
            File::open(&ca_file)
                .with_context(|| format!("Failed to open ca_file: {} ", ca_file.display()))?,
        );
        let results: Vec<_> = certs(&mut cert_file)
            .context("Failed to read certs from ca file")?
            .into_iter()
            .map(|certificate| {
                root_store
                    .add(&rustls::Certificate(certificate))
                    .context("Failed to add a certificate to root_store")
            })
            .collect();
        for result in results {
            if let Err(err) = result {
                return Err(err);
            }
        }
    } else {
        root_store.add_trust_anchors(TLS_SERVER_ROOTS.0.iter().map(|trust_anchor| {
            OwnedTrustAnchor::from_subject_spki_name_constraints(
                trust_anchor.subject,
                trust_anchor.spki,
                trust_anchor.name_constraints,
            )
        }));
    }

    let config = ClientConfig::builder()
        .with_safe_defaults()
        .with_root_certificates(root_store)
        .with_no_client_auth();
    Ok(config)
}
