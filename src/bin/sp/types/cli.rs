use std::{ops::RangeInclusive, path::PathBuf};

use clap::{Parser, Subcommand, ValueEnum};

/// A server maintenance tool, build to be precise and secure
#[derive(Parser, Debug, Clone)]
#[clap(author, version, about, long_about = None)]
#[command(next_line_help = true)]
pub struct Args {
    /// Which ip version to use
    #[arg(value_parser, long, short, default_value_t = IpVersion::V4)]
    pub ip_version: IpVersion,

    /// Domain and optional port of a host to talk to,
    /// can be specified multiple times to connect to each one.
    /// If no port is supplied, the default (9312 for authorized, 9311 for certificate request)
    /// is used.
    ///
    /// The domain should be separated from the port by a colon, e.g.:
    /// 'localhost:2312' or 'localhost', for the default
    #[arg(long="host", short='u', action = clap::ArgAction::Append, value_parser = parse_host_to_tuple)]
    pub hosts: Vec<(String, Option<u16>)>,

    /// Certificate to use, otherwise the well trusted CAs are used
    #[arg(long, short, value_parser)]
    pub ca_file: Option<PathBuf>,

    /// Optional fingerprint of the public ssh key used for signing, is only relevant, if the ssh
    /// agent contains more than one key
    #[arg(long, short = 'f', value_parser)]
    pub public_key_fingerprint: Option<String>,

    #[command(subcommand)]
    pub command: Command,
}
#[derive(Subcommand, Debug, Clone)]
pub enum Command {
    /// Run a command on the server, this requires the requested certificates
    #[clap(value_parser)]
    Authorized {
        //// The path to the certificate used in the client authentication
        #[arg(long, short, value_parser)]
        certificate: PathBuf,

        /// The path to the private key, used in the client authentication
        #[arg(long, short, value_parser)]
        private_key: PathBuf,


        /// The command to run
        #[command(subcommand)]
        command: AuthorizedCommand,
    },

    /// Request a new certificate from the server
    #[clap(value_parser)]
    RequestCertificate {
        /// Path to the key output file
        #[arg(long, short, value_parser)]
        key_output_file: PathBuf,

        /// Path to the certificate or certificate request output file
        #[arg(long, short, value_parser)]
        certificate_output_file: PathBuf,
    },
}

#[derive(Subcommand, Debug, Clone, Copy)]
pub enum AuthorizedCommand {
    /// Deploy the newest configuration
    #[clap(value_parser)]
    Deploy {},
}

#[derive(ValueEnum, Debug, Clone, Copy)]
pub enum IpVersion {
    /// ipv4
    V4,
    /// ipv6
    V6,
}
impl ToString for IpVersion {
    fn to_string(&self) -> String {
        match self {
            IpVersion::V4 => "v4".to_owned(),
            IpVersion::V6 => "v6".to_owned(),
        }
    }
}

// taken from the clap documentation
const PORT_RANGE: RangeInclusive<usize> = 1..=65535;

fn port_in_range(s: &str) -> Result<u16, String> {
    let port: usize = s
        .parse()
        .map_err(|_| format!("`{s}` isn't a port number"))?;
    if PORT_RANGE.contains(&port) {
        Ok(port as u16)
    } else {
        Err(format!(
            "port not in range {}-{}",
            PORT_RANGE.start(),
            PORT_RANGE.end()
        ))
    }
}

fn parse_host_to_tuple(s: &str) -> Result<(String, Option<u16>), String> {
    let strings: Vec<_> = s.split(':').collect();
    if strings.len() != 2 {
        if strings.len() > 2 {
            return Err(format!(
                "The host string ('{}') is invalid, as multiple colon exist; only one is allowed.",
                s
            ));
        } else {
            return Ok((
                strings
                    .get(0)
                    .expect("We checked the length above")
                    .to_string(),
                None,
            ));
        }
    } else {
        let domain = strings
            .get(0)
            .expect("We checked the length above")
            .to_string();
        let port = match port_in_range(strings.get(1).expect("We checked the length above")) {
            Ok(ok) => ok,
            Err(err) => return Err(err),
        };
        return Ok((domain, Some(port)));
    }
}
