use std::{net::SocketAddr, sync::Arc};

use anyhow::{Context, Result};
use tokio::net::TcpStream;
use tokio_rustls::{client::TlsStream, rustls, TlsConnector};

pub struct TlsClient {
    pub stream: TlsStream<TcpStream>,
}

impl TlsClient {
    pub async fn new(
        addr: SocketAddr,
        server_name: rustls::ServerName,
        config: Arc<rustls::ClientConfig>,
    ) -> Result<Self> {
        let tcp_stream = TcpStream::connect(addr)
            .await
            .with_context(|| format!("Failed to connect to {}", addr))?;
        let connector = TlsConnector::from(config);
        let stream = connector
            .connect(server_name, tcp_stream)
            .await
            .context("Failed creating client connection")?;

        Ok(Self { stream })
    }
}
