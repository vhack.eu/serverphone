use std::{str::FromStr, sync::Arc};

use anyhow::{Context, Result};
use clap::Parser;
use sptp::{
    certificate_request::User,
    tls::{load_certs, load_keys},
};
use thrussh_keys::{key::PublicKey, parse_public_key_base64};
use tokio::fs;

use crate::{
    server::{authorized, certificate_request, get_addrs},
    types::cli::Args,
};

pub mod server;
pub mod types;

#[tokio::main]
async fn main() -> Result<()> {
    let args = Args::parse();

    let tls_key = load_keys(&args.key).context("Failed to extract key")?;
    let certs = load_certs(&args.certificate).context("Failed to extract certificates")?;

    let ca_key = Arc::new(
        fs::read_to_string(&args.certificate_request.ca_key)
            .await
            .context("Failed to extract ca key")?,
    );
    let ca_certs = Arc::new(
        fs::read_to_string(&args.authorized.ca_certificate)
            .await
            .context("Failed to extract ca certificates")?,
    );

    let ca_certs_authorized =
        load_certs(&args.authorized.ca_certificate).context("Failed to extract ca certificates")?;

    let (authorized_server, certificate_request_server) = (
        authorized::generate_server(tls_key.clone(), certs.clone(), ca_certs_authorized)?,
        certificate_request::generate_server(tls_key, certs)?,
    );

    let (authorized_addrs, certificate_request_addrs) = (
        get_addrs(args.domain.clone(), args.authorized.port).await?,
        get_addrs(args.domain, args.certificate_request.cert_req_port).await?,
    );

    let accepted_public_ssh_keys =
        parse_ssh_keys(args.accepted_ssh_keys).context("Failed to parse ssh keys")?;
    let accepted_users =
        parse_users(args.certificate_request.accepted_users).context("Failed to parse users")?;

    let mut results = vec![];
    authorized_addrs.for_each(|socket_addr| {
        let local_authorized_server = authorized_server.clone();
        let authentication_command = args.authentication_command.clone();
        results.push(tokio::spawn(authorized::start_server(
            args.command,
            socket_addr,
            local_authorized_server,
            Arc::clone(&accepted_public_ssh_keys),
            args.authorized.config_repo_dir.clone(),
            authentication_command,
        )));
    });
    certificate_request_addrs.for_each(|socket_addr| {
        let local_certificate_request_server = certificate_request_server.clone();
        results.push(tokio::spawn(certificate_request::start_server(
            args.command,
            socket_addr,
            local_certificate_request_server,
            Arc::clone(&accepted_public_ssh_keys),
            Arc::clone(&accepted_users),
            Arc::clone(&ca_certs),
            Arc::clone(&ca_key),
            args.certificate_request.duration,
        )));
    });
    for result in results {
        if let Err(err) = result.await.context("Failed to join task")? {
            return Err(err);
        }
    }
    Ok(())
}
fn parse_ssh_keys(keys: Vec<String>) -> Result<Arc<Vec<PublicKey>>> {
    let maybe_parsed_keys: Vec<Result<PublicKey>> = keys
        .iter()
        .map(|key| -> Result<PublicKey> {
            return Ok(parse_public_key_base64(&key)
                .with_context(|| format!("Failed to parse public key: {}", key))?);
        })
        .collect();

    let mut parsed_keys = Vec::with_capacity(maybe_parsed_keys.len());

    for key in maybe_parsed_keys {
        parsed_keys.push(key?);
    }
    Ok(Arc::new(parsed_keys))
}

fn parse_users(users: Vec<String>) -> Result<Arc<Vec<User>>> {
    let maybe_parsed_users: Vec<Result<User>> = users
        .iter()
        .map(|user| -> Result<User> {
            return Ok(
                User::from_str(user).with_context(|| format!("Failed to parse user: {}", user))?
            );
        })
        .collect();

    let mut parsed_users = Vec::with_capacity(maybe_parsed_users.len());

    for user in maybe_parsed_users {
        parsed_users.push(user?);
    }
    Ok(Arc::new(parsed_users))
}

#[cfg(test)]
mod test {
    use thrussh_keys::parse_public_key_base64;

    #[test]
    fn parse_public_key() {
        // ssh-ed25519
        let public_key_64 = "AAAAC3NzaC1lZDI1NTE5AAAAIBCtvfOw/74uibWuLbwyH+vjvgbWlt7g6y36b5ai13w2";
        let public_key_fingerprint_64 = "7fnAXICyzygJRHaRoVIAwBNxt6mVCqsZTN149iJHjsI";

        let key = parse_public_key_base64(public_key_64).unwrap();
        assert_eq!(key.fingerprint(), public_key_fingerprint_64);
    }
}
