use std::{ops::RangeInclusive, path::PathBuf};

use clap::{arg, Args as ClapArgs, Parser, Subcommand};

/// A server maintenance tool, build to be precise and secure
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
#[command(next_line_help = true)]
pub struct Args {
    /// Domain to use
    #[arg(long, short, value_parser)]
    pub domain: String,

    /// Path to the tls cert file
    #[arg(long, short, value_parser)]
    pub certificate: PathBuf,

    /// Path to the tls key file
    #[arg(long, short, value_parser)]
    pub key: PathBuf,

    /// Command to use for authentication as root user
    #[arg(long, short='j', value_parser)]
    pub authentication_command: String,

    /// accepted public ssh keys, can be specified multiple times.
    ///
    /// Please only add the base64 part of the key, i.e., leave out the part in brackets:
    ///
    /// (ssh-ed25519 )AAAAC3NzaC1lZDI1NTE5AAAAIBCtvfOw/74uibWuLbwyH+vjvgbWlt7g6y36b5ai13w2(
    /// user@host)
    #[arg(long="accepted-ssh-key", short, action = clap::ArgAction::Append)]
    pub accepted_ssh_keys: Vec<String>,

    #[clap(flatten)]
    pub authorized: Authorized,

    #[clap(flatten)]
    pub certificate_request: CertificateRequest,

    #[command(subcommand)]
    pub command: Command,
}

#[derive(Subcommand, Debug, Clone, Copy)]
pub enum Command {
    /// Output all received input to stdout
    #[clap(value_parser)]
    Output {},

    /// Listen for new connections and act on their commands
    #[clap(value_parser)]
    Daemon {},
}

#[derive(Debug, Clone, ClapArgs)]
pub struct Authorized {
    /// Port to listen on for default operation
    #[arg(long="authorized-port", short='t', value_parser = port_in_range, default_value_t = 9312)]
    pub port: u16,

    /// The path to the nixos config repository
    #[arg(long, short='w', value_parser)]
    pub config_repo_dir: PathBuf,

    /// The path to ca root certificate to use for signing and verfing client certificates
    #[arg(long, short = 'f', value_parser)]
    pub ca_certificate: PathBuf,
}

#[derive(Debug, Clone, ClapArgs)]
pub struct CertificateRequest {
    /// Users eligible for a certificate request (and thus also a password authorization),
    /// can be specified multiple times.
    ///
    /// Please specify the username separated by a space from password hash in the same
    /// argument, e.g.:
    ///
    /// --accepted_user 'USERNAME PASSWORD_HASH'
    #[arg(long="accepted-user", short='u', action = clap::ArgAction::Append)]
    pub accepted_users: Vec<String>,

    /// Port to listen on for certificate requests
    #[arg(long="certificate-request-port", short='p', value_parser = port_in_range, default_value_t = 9311)]
    pub cert_req_port: u16,

    /// The path to ca private key to use for signing the client certificate requests
    #[arg(long, short = 'e', value_parser)]
    pub ca_key: PathBuf,

    /// The duration in days signed client certificates will be valid
    #[arg(long, short = 'i', value_parser)]
    pub duration: i64,
}

// taken from the clap documentation
const PORT_RANGE: RangeInclusive<usize> = 1..=65535;

fn port_in_range(s: &str) -> Result<u16, String> {
    let port: usize = s
        .parse()
        .map_err(|_| format!("`{s}` isn't a port number"))?;
    if PORT_RANGE.contains(&port) {
        Ok(port as u16)
    } else {
        Err(format!(
            "port not in range {}-{}",
            PORT_RANGE.start(),
            PORT_RANGE.end()
        ))
    }
}
