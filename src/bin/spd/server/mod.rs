use std::{net::SocketAddr, ops::Deref, sync::Arc};

use anyhow::{anyhow, Context, Result};
use sptp::{
    message::{
        package::{GetNextMessage, Package, PackageExt},
        status::Status,
    },
    ssh_authorization::{SshAuthorization, SshAuthorizationAnswer},
};
use thrussh_keys::key::PublicKey;
use tokio::{
    io::{split, AsyncRead, AsyncWrite, AsyncWriteExt},
    net::{self, TcpStream},
};
use tokio_rustls::TlsAcceptor;

pub mod authorized;
pub mod certificate_request;

pub async fn run_connection_in_output(
    peer_addr: SocketAddr,
    acceptor: TlsAcceptor,
    stream: TcpStream,
    accepted_ssh_keys: Arc<Vec<PublicKey>>,
) {
    let fut = async move {
        eprintln!("Got new connection with {}!", peer_addr);
        let stream = acceptor.accept(stream).await?;

        let (mut reader, mut writer) = split(stream);
        eprintln!("Sending challenge..");
        ssh_authorize_connection(&mut reader, &mut writer, accepted_ssh_keys).await?;
        eprintln!("They are now authorized");
        eprintln!("Start of messages");
        eprintln!("------------------");

        while let Ok(re) = reader.next_message().await {
            println!("Received a message: {}", re);
        }

        eprintln!("------------------");
        eprintln!("End of messages");
        println!("Bye peer {}!", peer_addr);

        Ok(()) as anyhow::Result<_>
    };

    if let Err(err) = fut.await {
        eprintln!("Encountered error: {:?}", err);
    }
}

async fn ssh_authorize_connection<
    R: AsyncRead + std::marker::Unpin + GetNextMessage,
    W: AsyncWrite + std::marker::Unpin + std::marker::Send,
>(
    reader: &mut R,
    mut writer: &mut W,
    accepted_ssh_keys: Arc<Vec<PublicKey>>,
) -> Result<()> {
    let ssh_authorization = SshAuthorization::new(accepted_ssh_keys.deref());
    ssh_authorization
        .generate_challenge()
        .send_as_package(&mut writer)
        .await
        .context("Failed to send challenge")?;

    let signed_value: SshAuthorizationAnswer = reader
        .next_message()
        .await
        .context("Failed to read answer to challenge")?
        .into();

    if ssh_authorization.validate_answer(&signed_value) {
        return Ok(());
    } else {
        writer
            .write_all(
                Status::SshVerificationFailed
                    .pack()
                    .context("Failed to package staus message")?
                    .as_bytes(),
            )
            .await
            .context("Failed to send status message")?;
        return Err(anyhow!("Client failed to authorize via ssh"));
    }
}
pub async fn get_addrs(
    domain: String,
    port: u16,
) -> Result<impl Iterator<Item = std::net::SocketAddr>> {
    let context_str = format!(
        "Failed to look up address for host '{}' on port '{}'",
        domain, port
    );
    Ok(net::lookup_host((domain, port))
        .await
        .context(context_str)?)
}
