use std::{net::SocketAddr, sync::Arc};

use anyhow::{anyhow, Context, Result};
use sptp::{
    certificate_request::{CertificateRequest, PasswordAuthorizationRequest, User},
    message::{
        package::{GetNextMessage, Package, PackageExt},
        status::Status,
    },
};
use thrussh_keys::key::PublicKey;
use tokio::{
    io::{split, AsyncRead, AsyncWrite, AsyncWriteExt},
    net::{TcpListener, TcpStream},
};
use tokio_rustls::{
    rustls::{Certificate, PrivateKey, ServerConfig},
    TlsAcceptor,
};

use crate::{server::ssh_authorize_connection, types::cli::Command};

use super::run_connection_in_output;

pub async fn start_server(
    command: Command,
    socket_addr: SocketAddr,
    server: TlsAcceptor,
    accepted_public_keys: Arc<Vec<PublicKey>>,
    accepted_users: Arc<Vec<User>>,
    ca_certificate_pem: Arc<String>,
    ca_certificate_private_key_pem: Arc<String>,
    duration_for_client_certificates: i64,
) -> Result<()> {
    let listener = TcpListener::bind(&socket_addr).await?;
    match command {
        Command::Output {} => loop {
            let (stream, peer_addr) = listener.accept().await?;
            let acceptor = server.clone();
            let accepted_public_keys = Arc::clone(&accepted_public_keys);

            tokio::spawn(async move {
                run_connection_in_output(peer_addr, acceptor, stream, accepted_public_keys).await;
            });
        },
        Command::Daemon {} => loop {
            let (stream, peer_addr) = listener.accept().await?;
            let acceptor = server.clone();
            let accepted_public_keys = Arc::clone(&accepted_public_keys);
            let accepted_users = Arc::clone(&accepted_users);
            let ca_certificate_pem = Arc::clone(&ca_certificate_pem);
            let ca_certificate_private_key_pem = Arc::clone(&ca_certificate_private_key_pem);

            tokio::spawn(async move {
                listen_for_cert_request(
                    peer_addr,
                    acceptor,
                    stream,
                    accepted_public_keys,
                    accepted_users,
                    ca_certificate_pem,
                    ca_certificate_private_key_pem,
                    duration_for_client_certificates,
                )
                .await;
            });
        },
    };
}

pub fn generate_server(key: PrivateKey, certs: Vec<Certificate>) -> Result<TlsAcceptor> {
    let config = Arc::new(
        ServerConfig::builder()
            .with_safe_defaults()
            .with_no_client_auth()
            .with_single_cert(certs, key)
            .context("Failed to generate certificate request server config")?,
    );
    Ok(TlsAcceptor::from(config))
}

async fn listen_for_cert_request(
    _peer_addr: SocketAddr,
    acceptor: TlsAcceptor,
    stream: TcpStream,
    accepted_ssh_keys: Arc<Vec<PublicKey>>,
    accepted_users: Arc<Vec<User>>,
    ca_certificate_pem: Arc<String>,
    ca_certificate_private_key_pem: Arc<String>,
    duration_for_client_certificates: i64,
) {
    let fut = async move {
        let stream = acceptor.accept(stream).await?;

        let (mut reader, mut writer) = split(stream);
        ssh_authorize_connection(&mut reader, &mut writer, accepted_ssh_keys).await?;
        password_authorize_connection(&mut reader, &mut writer, accepted_users).await?;

        let certificate_request: CertificateRequest = reader
            .next_message_deserialized()
            .await
            .context("Failed to read certificate request")?;

        let certificate = match certificate_request
            .fulfill_request(
                &*ca_certificate_pem,
                &*ca_certificate_private_key_pem,
                duration_for_client_certificates,
            )
            .context("Failed to sign certificate")
        {
            Ok(ok) => ok,
            Err(err) => {
                Status::ConnectionFailed(format!("{:?}", err))
                    .send_as_package(&mut writer)
                    .await
                    .context("Failed to send failure message")?;
                return Err(err);
            }
        };
        certificate
            .send_as_package(&mut writer)
            .await
            .context("Failed to answer with signed certificate")?;

        Status::ConnectionSucceeded
            .send_as_package(&mut writer)
            .await
            .context("Failed to send success message")?;
        Ok(()) as anyhow::Result<_>
    };

    if let Err(err) = fut.await {
        eprintln!("Encountered error: {:?}", err);
    }
}
async fn password_authorize_connection<R, W>(
    reader: &mut R,
    writer: &mut W,
    accepted_users: Arc<Vec<User>>,
) -> Result<()>
where
    R: AsyncRead + std::marker::Unpin + GetNextMessage + std::marker::Send,
    W: AsyncWrite + std::marker::Unpin,
{
    let password_authorization_request: PasswordAuthorizationRequest = reader
        .next_message_deserialized()
        .await
        .context("Failed to read password authorization request")?;

    if password_authorization_request.verify_request(&*accepted_users) {
        return Ok(());
    } else {
        writer
            .write_all(
                Status::PasswordVerificationFailed
                    .pack()
                    .context("Failed to package staus message")?
                    .as_bytes(),
            )
            .await
            .context("Failed to send status message")?;
        return Err(anyhow!("Client failed to authorize via password"));
    }
}
