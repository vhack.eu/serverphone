use std::{net::SocketAddr, path::PathBuf, sync::Arc};

use anyhow::{Context, Result};
use sptp::{
    message::{
        package::{GetNextMessage, PackageExt},
        status::Status,
    },
    sptp_command::SptpCommand,
};
use thrussh_keys::key::PublicKey;
use tokio::{io::split, net::TcpListener};
use tokio_rustls::{
    rustls::{
        server::AllowAnyAuthenticatedClient, Certificate, PrivateKey, RootCertStore, ServerConfig,
    },
    TlsAcceptor,
};

use crate::types::cli::Command;

use self::authorized_functions::deploy::deploy;

use super::{run_connection_in_output, ssh_authorize_connection};

pub mod authorized_functions;

pub async fn start_server(
    command: Command,
    socket_addr: SocketAddr,
    server: TlsAcceptor,
    accepted_public_keys: Arc<Vec<PublicKey>>,
    repository_path: PathBuf,
    authentication_command: String,
) -> Result<()> {
    let listener = TcpListener::bind(&socket_addr).await?;
    match command {
        Command::Output {} => loop {
            let (stream, peer_addr) = listener.accept().await?;
            let acceptor = server.clone();
            let accepted_public_keys = Arc::clone(&accepted_public_keys);

            tokio::spawn(async move {
                run_connection_in_output(peer_addr, acceptor, stream, accepted_public_keys).await;
            });
        },
        Command::Daemon {} => loop {
            let (stream, _) = listener.accept().await?;
            let acceptor = server.clone();
            let accepted_public_keys = Arc::clone(&accepted_public_keys);
            let repository_path = repository_path.clone();
            let authentication_command = authentication_command.clone();

            let fut = async move {
                let stream = acceptor.accept(stream).await?;

                let (mut reader, mut writer) = split(stream);
                ssh_authorize_connection(&mut reader, &mut writer, accepted_public_keys).await?;

                let command: SptpCommand = reader
                    .next_message_deserialized()
                    .await
                    .context("Failed to read command")?;

                let command_error;
                match command {
                    SptpCommand::Deploy => {
                        command_error = deploy(repository_path, &mut writer, &authentication_command)
                            .await
                            .context("Failed to deploy")
                    }
                }

                if let Err(err) = command_error {
                    let _ = Status::ConnectionFailed(format!("{:?}", err))
                        .send_as_package(&mut writer)
                        .await;
                    return Err(err);
                } else {
                    Status::ConnectionSucceeded
                        .send_as_package(&mut writer)
                        .await
                        .context("Failed to send success message")?;
                    return Ok(()) as anyhow::Result<_>;
                }
            };

            tokio::spawn(async move {
                if let Err(err) = fut.await {
                    eprintln!("Encountered error: {:?}", err);
                }
            });
        },
    };
}

pub fn generate_server(
    key: PrivateKey,
    certs: Vec<Certificate>,
    ca_certs: Vec<Certificate>,
) -> Result<TlsAcceptor> {
    let mut root_store = RootCertStore::empty();
    for cert in ca_certs {
        root_store
            .add(&cert)
            .context("Failed to add certificate to root certificate store")?;
    }

    let config = Arc::new(
        ServerConfig::builder()
            .with_safe_defaults()
            .with_client_cert_verifier(Arc::new(AllowAnyAuthenticatedClient::new(root_store)))
            .with_single_cert(certs, key)
            .context("Failed to generate authorized request server config")?,
    );
    Ok(TlsAcceptor::from(config))
}
