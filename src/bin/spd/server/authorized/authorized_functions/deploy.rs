use std::{collections::HashMap, env, path::PathBuf};

use anyhow::{Context, Result};
use sptp::message::{package::PackageExt, status::Status};
use tokio::io::AsyncWrite;

use super::run_external_command;

pub async fn deploy<W: AsyncWrite + std::marker::Send + std::marker::Unpin>(
    repository_path: PathBuf,
    mut writer: &mut W,
    authentication_command: &str,
) -> Result<()> {
    // libgit2 {{{
    //    let repository = Repository::open(&repository_path)
    //        .with_context(|| format!("Failed to open repository at {}", repository_path.display()))?;
    //
    //    repository
    //        .find_remote("origin")
    //        .context("Failed to find remote origin")?
    //        .fetch(
    //            &["prod"],
    //            None,
    //            None, /* TODO This could also be "pull", as per their 101 examples*/
    //        )
    //        .context("Failed to fetch prod branch")?;
    //    let head = repository
    //        .find_annotated_commit(
    //            repository
    //                .head()
    //                .context("Failed to get HEAD")?
    //                .peel_to_commit()
    //                .context("Failed to peel head to commit")?
    //                .as_object()
    //                .id(),
    //        )
    //        .context("Failed to find annotated commit for HEAD oid")?;
    //    let (tx, rx) = mpsc::channel();
    //    repository
    //        .fetchhead_foreach(
    //            |reference_name, _remote_url, reference_target_oid, _result_of_merge| -> bool {
    //                if reference_name == "prod" {
    //                    tx.send(reference_target_oid.clone())
    //                        .expect("The reciever is still recieving, it should always work");
    //                }
    //                true // TODO Is this c error handling? Why do I have to return a bool?
    //            },
    //        )
    //        .context("Failed to run callback for each FETCH HEAD")?;
    //
    //    let fetch_head = repository
    //        .find_annotated_commit(rx.recv().context("Failed to get FETCH HEAD oid")?)
    //        .context("Failed to find annotated commit for FETCH HEAD oid")?;
    //    repository
    //        .rebase(Some(&head), Some(&fetch_head), Some(&head), None)
    //        .context("Failed to rebase changes")?;
    //
    // }}}
    Status::CommandOutputStart
        .send_as_package(&mut writer)
        .await
        .context("Failed to signal command output start")?;

    let environment_variables: HashMap<String, String> = env::vars()
        .filter(|&(ref k, _)| k == "TZ" || k == "LANG" || k == "PATH")
        .collect();

    run_external_command(
        "git",
        &["pull", "--progress"],
        &repository_path,
        Some(&environment_variables),
        &mut writer,
    )
    .await?;

    run_external_command(
        "git",
        &["verify-commit", "HEAD"],
        &repository_path,
        Some(&environment_variables),
        &mut writer,
    )
    .await
    .context("Failed to verify signature")?;

    run_external_command(
        authentication_command,
        &["nixos-rebuild", "switch"],
        &repository_path,
        Some(&environment_variables),
        &mut writer,
    )
    .await
    .context("Failed to rebuild the system")?;

    Status::CommandOutputEnd
        .send_as_package(&mut writer)
        .await
        .context("Failed to signal command output start")?;
    Ok(())
}

#[cfg(test)]
mod test {
    use sptp::message::package::GetNextMessage;
    use sptp::message::status::Status;
    use sptp::sptp_command::SptpCommandOutput;
    use tokio::io::{AsyncBufReadExt, BufReader};
    use tokio::net::UnixStream;
    use tokio::process::Command;

    use std::process::Stdio;

    use crate::authorized::authorized_functions::deploy::deploy;

    //#[tokio::test]
    #[allow(dead_code)]
    async fn test_deploy() {
        let tempdir = tempfile::tempdir().unwrap();

        let mut child = Command::new("git")
            .stderr(Stdio::piped())
            .args([
                "clone",
                "--progress",
                "https://codeberg.org/soispha/generate_firefox_extension.git",
                tempdir.path().to_str().unwrap(),
            ])
            .spawn()
            .unwrap();

        let stdout = child
            .stderr
            .take()
            .expect("child did not have a handle to stdout");

        let mut reader = BufReader::new(stdout).lines();

        tokio::spawn(async move {
            child.wait().await.unwrap();
        });

        while let Some(line) = reader.next_line().await.unwrap() {
            println!("Line: {}", line);
        }

        let (stream1, stream2) = UnixStream::pair().unwrap();
        let (_, mut writer) = stream1.into_split();
        let (mut reader2, _) = stream2.into_split();

        deploy(tempdir.path().to_owned(), &mut writer, "sudo")
            .await
            .unwrap();
        assert_eq!(
            Status::CommandOutputStart,
            reader2.next_message_deserialized().await.unwrap()
        );

        let mut reading = true;
        while reading {
            let line = reader2.next_message().await.unwrap();
            if let Ok(Status::CommandOutputEnd) = ron::from_str(&line) {
                reading = false;
            } else if let Ok(SptpCommandOutput::Stdout(line)) = ron::from_str(&line) {
                println!("Line was recieved on stdout: {}", line);
            } else if let Ok(SptpCommandOutput::Stdout(line)) = ron::from_str(&line) {
                eprintln!("Line was recieved on stderr: {}", line);
            } else {
                unreachable!("everything else would be a protocol error");
            }
        }
    }
}
