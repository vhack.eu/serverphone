use std::{path::Path, process::Stdio, collections::HashMap};

use anyhow::{anyhow, Context, Result};
use sptp::{message::package::PackageExt, sptp_command::SptpCommandOutput};
use tokio::{
    io::{AsyncBufReadExt, AsyncWrite, BufReader},
    process, select,
};

pub mod deploy;

pub async fn run_external_command<W: AsyncWrite + std::marker::Send + std::marker::Unpin>(
    command_name: &str,
    command_args: &[&str],
    repository_path: &Path,
    environment_variables: Option<&HashMap<String, String>>,
    mut writer: &mut W,
) -> Result<()> {
    let empty_hash_map = HashMap::new();
    let env_vars = environment_variables.unwrap_or(&empty_hash_map);
    let command_display_name = format!("{} {}", command_name, command_args.join(" "));
    let mut child = process::Command::new(command_name)
        .args(command_args)
        // TODO this is important for security reasons:
        // .env_clear()
        .envs(env_vars)
        .stderr(Stdio::piped())
        .stdout(Stdio::piped())
        .current_dir(repository_path)
        .spawn()
        .with_context(|| format!("Failed to start `{}`", command_display_name))?;

    let (stderr, stdout) = (
        child
            .stderr
            .take()
            .with_context(|| format!("Failed to take `{}` stderr", command_display_name))?,
        child
            .stdout
            .take()
            .with_context(|| format!("Failed to take `{}` stdout", command_display_name))?,
    );

    let mut err_lines = BufReader::new(stderr).lines();
    let mut out_lines = BufReader::new(stdout).lines();

    let res = tokio::spawn(async move {
        let status = child
            .wait()
            .await
            .with_context(|| format!("Failed to wait for `{}`", command_display_name))?;
        if !status.success() {
            Err(anyhow!("Command failed"))
                .with_context(|| format!("Failed to run `{}`", command_display_name))?
        } else {
            Ok::<(), anyhow::Error>(())
        }
    });

    while let Some(line) = select!(
        error_line = err_lines
            .next_line()
             => {
            if let Some(line) = error_line.context("Failed to read next error string")? {
                Some(SptpCommandOutput::Stderr(line))
            } else {
                None
            }
        },
        out_line = out_lines
            .next_line()
             => {
            if let Some(line) = out_line.context("Failed to read next out string")? {
                Some(SptpCommandOutput::Stdout(line))
            } else {
                None
            }
        },
    ) {
        line.send_as_package(&mut writer)
            .await
            .context("Failed to send line")?;
    }

    res.await.context("Failed to join task")??;
    Ok(())
}

