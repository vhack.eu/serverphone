use std::path::PathBuf;

use clap::{Parser, Subcommand};

/// A tool useful when programming with tls
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
#[command(next_line_help = true)]
pub struct Args {
    #[command(subcommand)]
    pub command: Command,
}
#[derive(Subcommand, Debug, Clone)]
pub enum Command {
    /// Output the content of a certificate, key or pem file
    #[clap(value_parser)]
    ScanFile {
        /// Path to the cert file
        #[arg(value_parser)]
        file: PathBuf,
    },
    /// generate certificates and secret keys
    #[clap(value_parser)]
    Generate {
        /// What to generate
        #[command(subcommand)]
        command: GenerateCommand,
    },
    /// hash a password to be used for spd certificate request authentication
    #[clap(value_parser)]
    Hash {
        /// Username to use, can't contain a space
        #[arg(value_parser = username_without_space)]
        username: String,
    },
}

#[derive(Subcommand, Debug, Clone)]
pub enum GenerateCommand {
    /// generate ca certificate and elliptic curve based secret key
    #[clap(value_parser)]
    CaCertificate {
        /// Path to the key output file
        #[arg(long, short, value_parser)]
        key_output_file: PathBuf,

        /// Path to the certificate or certificate request output file
        #[arg(long, short, value_parser)]
        certificate_output_file: PathBuf,

        /// subject alternative name for the certificate, can be specified multiple times
        #[arg(long, short, action = clap::ArgAction::Append)]
        subject_alternative_names: Vec<String>,

        /// Duration in days the certificate will be valid for
        #[arg(long, short, value_parser)]
        duration: i64,
    },
    /// generate an certificate sign request and elliptic curve based secret key
    #[clap(value_parser)]
    CertificateSignRequest {
        /// Path to the key output file
        #[arg(long, short, value_parser)]
        key_output_file: PathBuf,

        /// Path to the certificate or certificate request output file
        #[arg(long, short, value_parser)]
        certificate_sign_request_output_file: PathBuf,

        /// subject alternative name for the certificate, can be specified multiple times
        #[arg(long, short, action = clap::ArgAction::Append)]
        subject_alternative_names: Vec<String>,
    },

    /// generate an certificate sign request and elliptic curve based secret key
    #[clap(value_parser)]
    SignCertificateSignRequest {
        /// The path to the certificate sign request
        #[arg(long, short = 'u', value_parser)]
        certificate_sign_request: PathBuf,

        /// The path to the ca certificate
        #[arg(long, short = 'a', value_parser)]
        ca_certificate: PathBuf,

        /// The path to the ca certificate private key
        #[arg(long, short, value_parser)]
        ca_certificate_private_key: PathBuf,

        /// The path to the signed certificate output file
        #[arg(long, short, value_parser)]
        signed_certificate_output_file: PathBuf,

        /// Duration in days the certificate will be valid for
        #[arg(long, short, value_parser)]
        duration: i64,
    },
}

fn username_without_space(s: &str) -> Result<String, String> {
    if !s.contains(' ') {
        Ok(s.to_owned())
    } else {
        Err(format!("username contains a Space (' '): {}", s,))
    }
}
