use std::{fs, path::PathBuf};

use anyhow::Result;
use rcgen::{
    BasicConstraints, Certificate, CertificateParams, CertificateSigningRequest, DnType, IsCa,
    KeyPair, SignatureAlgorithm,
};
use time::{ext::NumericalDuration, OffsetDateTime};

const PKCS_ECDSA_P256_SHA256_OID: &[u64] = &[1, 2, 840, 10045, 4, 3, 2];

pub fn ca_certificate(
    subject_alternative_names: Vec<String>,
    duration: i64,
) -> Result<(String, String)> {
    let algorithm = SignatureAlgorithm::from_oid(PKCS_ECDSA_P256_SHA256_OID)?;

    let mut certificate_params = CertificateParams::new(subject_alternative_names);
    let time_now = OffsetDateTime::now_utc();

    certificate_params.alg = algorithm;
    certificate_params.not_before = time_now;
    certificate_params.not_after = time_now.saturating_add(duration.days());
    certificate_params
        .distinguished_name
        .push(DnType::CommonName, "Serverphone CA");

    let allowed_length_of_the_intermediate_certificate_chain = 1;
    certificate_params.is_ca = IsCa::Ca(BasicConstraints::Constrained(
        allowed_length_of_the_intermediate_certificate_chain,
    ));

    let certificate = Certificate::from_params(certificate_params)?;
    let secret_key_str = certificate.serialize_private_key_pem();
    let certificate_str = certificate.serialize_pem()?;

    Ok((secret_key_str, certificate_str))
}
pub fn certificate_request(subject_alternative_names: Vec<String>) -> Result<(String, String)> {
    let algorithm = SignatureAlgorithm::from_oid(PKCS_ECDSA_P256_SHA256_OID)?;

    let mut certificate_params = CertificateParams::new(subject_alternative_names.clone());

    certificate_params.alg = algorithm;
    certificate_params.distinguished_name.push(
        DnType::CommonName,
        format!(
            "Client running on {}",
            subject_alternative_names
                .get(0)
                .unwrap_or(&"a network connection".to_owned())
        ),
    );

    let certificate = Certificate::from_params(certificate_params)?;
    let secret_key_str = certificate.serialize_private_key_pem();
    let certificate_str = certificate.serialize_request_pem()?;

    Ok((secret_key_str, certificate_str))
}

pub fn signed_certificate_request(
    certificate_sign_request: PathBuf,
    ca_certificate: PathBuf,
    ca_certificate_private_key: PathBuf,
    duration: i64,
) -> Result<String> {
    let certificate_signing_request_str = fs::read_to_string(certificate_sign_request)?;
    let mut certificate_signing_request =
        CertificateSigningRequest::from_pem(&certificate_signing_request_str)?;

    let time_now = OffsetDateTime::now_utc();

    certificate_signing_request.params.not_after = time_now.saturating_add(duration.days());
    certificate_signing_request.params.not_before = time_now;

    let ca_certificate_key_str = fs::read_to_string(ca_certificate_private_key)?;
    let ca_certificate_key = KeyPair::from_pem(&ca_certificate_key_str)?;

    let ca_certificate_str = fs::read_to_string(ca_certificate)?;
    let ca_certificate_params =
        CertificateParams::from_ca_cert_pem(&ca_certificate_str, ca_certificate_key)?;
    let ca_certificate = Certificate::from_params(ca_certificate_params)?;

    let signed_certificate_str =
        certificate_signing_request.serialize_pem_with_signer(&ca_certificate)?;

    Ok(signed_certificate_str)
}

// openssl {{{
//fn signed_certificate_request_openssl(
//    certificate_sign_request: PathBuf,
//    ca_certificate: PathBuf,
//    ca_certificate_private_key: PathBuf,
//) -> Result<()> {
//    fn turn_file_vec(file: PathBuf) -> Result<Vec<u8>> {
//        let mut opened_file = File::open(file)?;
//        let mut return_vec = vec![];
//        opened_file.read_to_end(&mut return_vec)?;
//        Ok(return_vec)
//    }
//    // taken from https://github.com/est31/rcgen/issues/78
//    // TODO rewrite this if rcgen supports this
//    let csr_as_pem = turn_file_vec(certificate_sign_request)?;
//    let csr = X509Req::from_pem(&csr_as_pem[..])?;
//
//    let ca_cert_as_pem = turn_file_vec(ca_certificate)?;
//    let ca_cert = X509::from_pem(&ca_cert_as_pem[..])?;
//
//    let ca_cert_priv_key_as_bytes = turn_file_vec(ca_certificate_private_key)?;
//    let ca_key = PKey::from_ec_key(EcKey::private_key_from_pem(&ca_cert_priv_key_as_bytes[..])?);
//
//    let time_now = OffsetDateTime::now_utc();
//    let not_before = time_now;
//    let not_after = csr.not_after();
//
//    let mut builder = X509::builder()?;
//    builder.set_version(request.version())?;
//    builder.set_subject_name(request.subject_name())?;
//    builder.set_pubkey(request.public_key()?.as_ref())?;
//    builder.set_not_before(not_before.as_ref())?;
//    builder.set_not_after(not_after.as_ref())?;
//
//    match request.extensions() {
//        Ok(extensions) => extensions
//            .iter()
//            .map(|ext| builder.append_extension2(ext))
//            .collect::<Result<Vec<_>, _>>()
//            .map(|_| ()),
//        _ => Ok(()),
//    }?;
//    builder.set_issuer_name(ca_cert.subject_name())?;
//    builder.set_serial_number(
//        Asn1Integer::from_bn(BigNum::from_u32(next_number)?.as_ref())?.as_ref(),
//    )?;
//    builder.sign(ca_key.as_ref(), MessageDigest::sha256())?;
//
//    Ok(())
//}
// }}}
