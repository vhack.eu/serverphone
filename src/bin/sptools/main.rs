use std::{
    fs::{self, File},
    io::BufReader,
};

use anyhow::{Context, Result};
use argon2::{
    password_hash::{rand_core::OsRng, SaltString},
    Argon2, PasswordHasher,
};
use clap::Parser;
use rpassword::prompt_password;
use rustls_pemfile::{read_all, Item};

use crate::types::cli::{Args, Command, GenerateCommand};

pub mod generate;
pub mod types;

fn main() -> Result<()> {
    let args = Args::parse();
    match args.command {
        Command::ScanFile { file } => {
            let mut reader = BufReader::new(File::open(file)?);
            let certs = read_all(&mut reader)?;

            println!("Listing found values:");
            // Assume `reader` is any std::io::BufRead implementor
            for item in certs {
                match item {
                    Item::X509Certificate(_) => println!("certificate"),
                    Item::RSAKey(_) => println!("rsa pkcs1 key"),
                    Item::PKCS8Key(_) => println!("pkcs8 key"),
                    Item::ECKey(_) => println!("sec1 ec key"),
                    _ => println!("unhandled item"),
                }
            }
        }
        Command::Generate { command } => {
            match command {
                GenerateCommand::CaCertificate {
                    key_output_file,
                    certificate_output_file,
                    subject_alternative_names,
                    duration,
                } => {
                    let (secret_key, certificate) =
                        generate::ca_certificate(subject_alternative_names, duration)?;
                    fs::write(key_output_file, secret_key)?;
                    fs::write(certificate_output_file, certificate)?;
                }
                GenerateCommand::CertificateSignRequest {
                    key_output_file,
                    certificate_sign_request_output_file,
                    subject_alternative_names,
                } => {
                    let (secret_key, certificate_request) =
                        generate::certificate_request(subject_alternative_names)?;

                    fs::write(key_output_file, secret_key)?;
                    fs::write(certificate_sign_request_output_file, certificate_request)?;
                }
                GenerateCommand::SignCertificateSignRequest {
                    certificate_sign_request,
                    ca_certificate,
                    ca_certificate_private_key,
                    signed_certificate_output_file,
                    duration,
                } => {
                    let signed_certificate = generate::signed_certificate_request(
                        certificate_sign_request,
                        ca_certificate,
                        ca_certificate_private_key,
                        duration,
                    )?;
                    fs::write(signed_certificate_output_file, signed_certificate)
                        .context("Failed to sign certificate sign request")?;
                }
            };
        }
        Command::Hash { username } => {
            let password = prompt_password("Your password: ").context("Failed to read password")?;
            let salt = SaltString::generate(&mut OsRng);
            let argon = Argon2::default();
            let password_hash = argon
                .hash_password(password.as_bytes(), &salt)
                .context("Failed to hash password")?
                .to_string();
            println!(
                "Username and password hash for spd: '{} {}'",
                username, password_hash
            );
        }
    };
    Ok(())
}
