#!/bin/sh

cargo update && cargo upgrade

cd sptp || (echo "No sptp subdirectory" && exit 1)

cargo update && cargo upgrade

cd ..
git add Cargo.lock Cargo.toml flake.lock sptp/Cargo.toml

# vim: ft=sh
